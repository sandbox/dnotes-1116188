<?php
// $Id$

/**
 * @file
 *
 */

function gmap_legend_settings_form() {
  $form['gmap_legend_node_types'] = array(
    '#type' => 'checkboxes',
    '#options' => node_get_types('names'),
    '#title' => t('Node types to include in gmap legends'),
    '#default_value' => variable_get('gmap_legend_node_types', array()),
  );
  $form['array_filter'] = array('#type' => 'hidden');
  $form['note'] = array(
    '#value' => '<div>' . t('Submitting this form should clear the cached data for the legend.') . '</div>'
  );
  $form['#submit'][] = 'gmap_legend_settings_submit';
  return system_settings_form($form);
}

function gmap_legend_settings_submit() {
  cache_clear_all('gmap_legend:view-gmap-key', 'cache');
}